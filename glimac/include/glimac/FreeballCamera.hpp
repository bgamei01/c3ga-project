#pragma once

#include <glm/ext.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>

class FreeballCamera {

public:
    FreeballCamera() :
            m_fTheta{0},
            m_fPhi{glm::pi<float>()},
            m_Position{glm::vec3(0, 0, 0)},
            m_fDistance{5},
            m_fAngleX{0},
            m_fAngleY{0} {
        computeDirectionVectors();
    }

    void moveLeft(float t) {
        m_Position += t * m_LeftVector;
    }

    void moveUp(float t) {
        m_Position += t * m_UpVector;
    }

    void turnLeft(float degrees) {
        m_fPhi += glm::radians(degrees);
    }

    void turnUp(float degrees) {
    m_fTheta += glm::radians(degrees);
    }

    void moveFront(float delta) {
        m_fDistance += delta;
    }

    void rotateLeft(float degrees) {
        m_fAngleY += degrees;
    }

    void rotateUp(float degrees) {
    m_fAngleX += degrees;
    }

    glm::mat4 getViewMatrix() {
        computeDirectionVectors();
        glm::mat4 M = glm::lookAt(m_Position, m_Position + m_FrontVector, m_UpVector);
        M = glm::rotate(M, glm::radians(m_fAngleX), m_LeftVector);
        M = glm::rotate(M, glm::radians(m_fAngleY), m_UpVector);
        return M;
    }

private:

    void computeDirectionVectors() {
        m_FrontVector = {glm::cos(m_fTheta) * glm::sin(m_fPhi), glm::sin(m_fTheta),
                         glm::cos(m_fTheta) * glm::cos(m_fPhi)};
        m_LeftVector = {glm::sin(m_fPhi + glm::half_pi<float>()), 0, glm::cos(m_fPhi + glm::half_pi<float>())};
        m_UpVector = glm::cross(m_FrontVector, m_LeftVector);
    }

    glm::vec3 m_Position;
    float m_fPhi;
    float m_fTheta;
    float m_fAngleX;
    float m_fAngleY;
    float m_fDistance;
    glm::vec3 m_FrontVector;
    glm::vec3 m_LeftVector;
    glm::vec3 m_UpVector;
};