#pragma once

#include <vector>

#include "common.hpp"

namespace glimac {

class Sphere {
    void build(GLfloat radius, GLsizei discLat, GLsizei discLong);

public:
    Sphere(GLfloat radius, GLsizei discLat, GLsizei discLong):
        m_nVertexCount(0) {
        build(radius, discLat, discLong);
    }

    // Renvoit le pointeur vers les données
    const ShapeVertex* getDataPointer() const {
        return &vertices[0];
    }
    
    // Renvoit le nombre de vertex
    GLsizei getVertexCount() const {
        return m_nVertexCount;
    }

private:
    std::vector<ShapeVertex> vertices;
    GLsizei m_nVertexCount; // Nombre de sommets
};
    
}