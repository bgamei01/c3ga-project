# C3GA Project

3D mini project using Conformal Geometric Algebra (cga or c3ga in short).

You can control a spaceship and visit different planets of different sizes and color.

This is mainly a POC that you can do stuff with cga easily (intersection, rotation, ...)

# Installation

Some libraries are already in the project but you need to have :
 - Eigen3 : Eigen 3.3.4  or more [Eigen](http://eigen.tuxfamily.org)
 - OpenGL : 4.3 or more
 - GLEW : 2.1.0 or more

I haven't tested this project on any computer other than mine so this might not work first time.

For more information about cga check the [repository of the library](https://github.com/vincentnozick/garamon) used.
The display part has been done based on project given by [Venceslas BIRI](https://twitter.com/vence_biri).
You can find the full project [here](https://gitlab.com/bgamei01/synthese-image)