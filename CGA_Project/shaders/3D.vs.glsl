#version 300 es

layout(location = 0) in vec3 aVertexPosition;

uniform mat4 uMVPMatrix;

void main() {
  vec4 position = vec4(aVertexPosition, 1);

  gl_Position = uMVPMatrix * position;
}
