#include "include/Application.hpp"
#include <GL/glut.h>
#include <cstdlib>

int main(int argc, char **argv) {
    Application application(argv[0]);
    glutInit(&argc, argv);
    application.run();
    return EXIT_SUCCESS;
}
