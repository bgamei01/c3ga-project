#pragma once

#include "c3ga/Mvec.hpp"
#include <glimac/glm.hpp>

c3ga::Mvec<double> point(const glm::vec3 &p) {
    c3ga::Mvec<double> mv;
    mv[c3ga::E1] = p.x;
    mv[c3ga::E2] = p.y;
    mv[c3ga::E3] = p.z;
    mv[c3ga::Ei] = 0.5 * (p.x * p.x + p.y * p.y + p.z * p.z);
    mv[c3ga::E0] = 1.0;

    return mv;
}

c3ga::Mvec<double> vector(const glm::vec3 &p) {
    c3ga::Mvec<double> mv;
    mv[c3ga::E1] = p.x;
    mv[c3ga::E2] = p.y;
    mv[c3ga::E3] = p.z;

    return mv;
}

c3ga::Mvec<double> makeSphere(const glm::vec3 &center, const double radius) {
    c3ga::Mvec<double> dualSphere = point(center);
    dualSphere[c3ga::Ei] -= 0.5 * radius * radius;
    return dualSphere;
}

void breakSphere(const c3ga::Mvec<double> &sphere, glm::vec3 &c, double &radius) {
    // back to correct scale
    c3ga::Mvec<double> mv = sphere / sphere[c3ga::E0];

    double squaredRadius = mv | mv;
    radius = sqrt(abs(squaredRadius));

    // extract center
    c3ga::Mvec<double> center = mv;
    center /= center[c3ga::E0];

    c = glm::vec3(mv[c3ga::E1], mv[c3ga::E2], mv[c3ga::E3]);
}
