#pragma once

#include <vector>

#include <glimac/glm.hpp>
#include <glm/ext.hpp>
#include <SDL.h>

#include "Star.hpp"
#include "Camera.hpp"

class PlayerController {

public:
    explicit PlayerController();

    bool treatEvent(const SDL_Event &e);

    glm::vec3 getPosition() const;

    glm::vec3 getDirection() const;

    bool paused() const;

    bool hasTarget() const;

    Star getTarget() const;

    double getDistanceToTarget() const;

    void resetTarget();

    void update(Camera &camera, std::vector<Star> &stars);

private:
    // Input holder
    glm::vec4 motion{0};
    // Display data
    bool pause{true};
    Star *target = NULL;
    double distanceToTarget{0};
    glm::vec3 position, direction;

    void sphereIntersect(std::vector<Star> &stars);
};