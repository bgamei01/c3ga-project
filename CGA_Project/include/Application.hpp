#pragma once

#include <glimac/SDLWindowManager.hpp>
#include <glimac/Sphere.hpp>
#include <GL/glew.h>

#include "PlayerController.hpp"
#include "Program3D.hpp"
#include "Camera.hpp"
#include "Star.hpp"

#define HEIGHT 800
#define WIDTH 1000

class Application {
public:
    Application(const char *execName);

    ~Application() = default;

    void run();

private:
    // Draw
    GLuint vao{}, vbo{};
    glm::mat4 ProjMatrix{1};
    glimac::Sphere unitSphere{1, 64, 32};

    // Data
    Camera camera{};
    std::vector<Star> stars{};
    PlayerController controller{};

    // Window
    std::unique_ptr<Program3D> program;
    glimac::SDLWindowManager windowManager{WIDTH, HEIGHT, "CGA application"};

    void initVboVao();

    void draw_sphere(const glm::vec3 &position, const glm::vec3 &color, double radius) const;

    void drawGUI() const;

    void createRandomStars(const std::string &execName);
};

