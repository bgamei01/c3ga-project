#pragma once

#include <iostream>
#include <glimac/glm.hpp>
#include "c3ga/Mvec.hpp"

#define MERGE_FACTOR 0.5

class Star {
public:
    Star(std::string name, const glm::vec3 &color, const glm::vec3 &center,
         c3ga::Mvec<double> rotationAxis, const double scale);

    glm::vec3 getPosition() const;

    glm::vec3 getColor() const;

    std::string getName() const;

    double getRadius() const;

    void update(double dt);

    // Return true on collision (if collision need to delete other on the calling method)
    bool isCollidingWith(const Star &other);

    double distanceTo(const glm::vec3& pos, const glm::vec3& direction) const;

private:
    // Draw data
    double radius{1};
    std::string name;
    glm::vec3 position{0}, color;

    // C3GA data
    c3ga::Mvec<double> rotationAxis, dualSphere;

    void updateDisplayData();

    void rescale(double scaleFactor);
};