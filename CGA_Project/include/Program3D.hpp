#pragma once

#include <glimac/Program.hpp>

class Program3D {
public:
    Program3D(const char *execName);

    glimac::Program program;
    GLuint programId{}, uColor{}, uMVPMatrix{};
};

