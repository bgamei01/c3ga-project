#pragma once

#include <glimac/glm.hpp>

class Camera {

public:
    Camera();

    void moveLeft(float t);

    void moveFront(float t);

    void rotateLeft(float degrees);

    void rotateUp(float degrees);

    glm::mat4 getViewMatrix() const;

    glm::vec3 getPosition() const;

    glm::vec3 getFrontVector() const;

    void setPosition(const glm::vec3& newPosition);

private:
    void computeDirectionVectors();

    float m_fPhi, m_fTheta;
    glm::vec3 m_Position, m_FrontVector, m_LeftVector, m_UpVector;
};