#include "../include/Star.hpp"
#include "../include/c3gaUtils.hpp"

#include <utility>

Star::Star(std::string name, const glm::vec3 &color, const glm::vec3 &center,
           c3ga::Mvec<double> rotationAxis, const double scale) :
        name{std::move(name)}, color{color}, rotationAxis{std::move(rotationAxis)},
        dualSphere{makeSphere(center, scale)} {
    updateDisplayData();
}

glm::vec3 Star::getPosition() const {
    return position;
}

glm::vec3 Star::getColor() const {
    return color;
}

std::string Star::getName() const {
    return name;
}

double Star::getRadius() const {
    return radius;
}

void Star::update(double dt) {
    double speed{radius < 1 ? dt : dt / radius};

    c3ga::Mvec<double> R = glm::cos(speed / 2) - rotationAxis * glm::sin(speed / 2);
    c3ga::Mvec<double> rotatedSphere = R * !dualSphere * R.inv();
    dualSphere = !!!rotatedSphere;
    updateDisplayData();
}

bool Star::isCollidingWith(const Star &other) {
    c3ga::Mvec<double> intersection = dualSphere ^other.dualSphere;
    double distance = intersection | intersection;
    if (distance > 0) {
        return false;
    } else {
        double oldRadius = radius;
        radius = fmax(radius, other.radius) + fmin(radius, other.radius) * MERGE_FACTOR;
        rescale(radius / oldRadius);
        updateDisplayData();
        return true;
    }
}

double Star::distanceTo(const glm::vec3 &pos, const glm::vec3 &direction) const {
    c3ga::Mvec<double> p = point(pos);
    c3ga::Mvec<double> u = vector(direction);
    c3ga::Mvec<double> ray = p ^u ^c3ga::ei<double>();
    c3ga::Mvec<double> D = !(dualSphere ^ (!ray));
    double D2 = D * D;
    if (D2 <= 0) return std::numeric_limits<double>::infinity();

    c3ga::Mvec<double> denominator = (-c3ga::ei<double>() | D);
    double sqrtD = sqrt(D2);
    c3ga::Mvec<double> p1 = (D + sqrtD) / denominator;
    c3ga::Mvec<double> p2 = (D - sqrtD) / denominator;
    double d1 = (p - p1).norm();
    double d2 = (p - p2).norm();
    return d1 >= d2 ? d1 : std::numeric_limits<double>::infinity();
}

void Star::rescale(double scaleFactor) {
    c3ga::Mvec<double> T1 = 1 - c3ga::ei<double>() * vector(position) / 2;
    c3ga::Mvec<double> S = 1 - c3ga::e0i<double>() * ((1.0 - scaleFactor) / (1.0 + scaleFactor));
    c3ga::Mvec<double> T2 = 1 - c3ga::ei<double>() * vector(-position) / 2;

    c3ga::Mvec<double> scaledSphere = T2 * S * T1 * !dualSphere * T1.inv() * S.inv() * T2.inv();
    dualSphere = !scaledSphere;
}

void Star::updateDisplayData() {
    breakSphere(dualSphere, position, radius);
}