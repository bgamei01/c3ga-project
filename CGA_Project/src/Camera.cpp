#include "../include/Camera.hpp"


using namespace glm;

Camera::Camera() :
        m_fPhi{pi<float>()},
        m_fTheta{0},
        m_Position{vec3(0, 0, 200)} {
    computeDirectionVectors();

}

void Camera::moveLeft(float t) {
    m_Position += t * m_LeftVector;
}

void Camera::moveFront(float t) {
    m_Position += t * m_FrontVector;
}

void Camera::rotateLeft(float degrees) {
    m_fPhi += radians(degrees);
    computeDirectionVectors();
}

void Camera::rotateUp(float degrees) {
    m_fTheta += radians(degrees);
    computeDirectionVectors();
}

mat4 Camera::getViewMatrix() const {
    return lookAt(m_Position, m_Position + m_FrontVector, m_UpVector);
}

vec3 Camera::getPosition() const {
    return m_Position;
}

vec3 Camera::getFrontVector() const {
    return m_FrontVector;
}

void Camera::setPosition(const vec3 &newPosition) {
    m_Position = newPosition;
}

void Camera::computeDirectionVectors() {
    m_FrontVector = {cos(m_fTheta) * sin(m_fPhi), sin(m_fTheta), cos(m_fTheta) * cos(m_fPhi)};
    m_LeftVector = {sin(m_fPhi + half_pi<float>()), 0, cos(m_fPhi + half_pi<float>())};
    m_UpVector = cross(m_FrontVector, m_LeftVector);
}
