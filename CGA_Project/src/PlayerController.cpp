#include "../include/PlayerController.hpp"

using namespace glm;

PlayerController::PlayerController() = default;

bool PlayerController::treatEvent(const SDL_Event &e) {
    if (e.type == SDL_QUIT) return true;

    if (e.type == SDL_MOUSEMOTION && e.motion.state & SDL_BUTTON_LMASK) {
        motion = {-e.motion.xrel, -e.motion.yrel, motion.z, motion.w};
    }
    if (e.type == SDL_KEYDOWN) {
        auto sym = e.key.keysym.sym;
        float amount = (e.key.keysym.mod & KMOD_CTRL) != 0 ? .2 : .1;

        if (sym == SDLK_UP || sym == SDLK_z) {
            motion = {motion.x, motion.y, motion.z, amount};
        } else if (sym == SDLK_DOWN || sym == SDLK_s) {
            motion = {motion.x, motion.y, motion.z, -amount};
        } else if (sym == SDLK_LEFT || sym == SDLK_q) {
            motion = {motion.x, motion.y, amount, motion.w};
        } else if (sym == SDLK_RIGHT || sym == SDLK_d) {
            motion = {motion.x, motion.y, -amount, motion.w};
        } else if (e.key.keysym.sym == SDLK_SPACE) {
            pause = !pause;
        }
    }
    return false;
}

vec3 PlayerController::getPosition() const {
    return position;
}

vec3 PlayerController::getDirection() const {
    return direction;
}

bool PlayerController::paused() const {
    return pause;
}

bool PlayerController::hasTarget() const {
    return target != nullptr;
}

Star PlayerController::getTarget() const {
    assert(target != nullptr);
    return *target;
}

double PlayerController::getDistanceToTarget() const {
    return distanceToTarget;
}

void PlayerController::resetTarget() {
    target = nullptr;
}

void PlayerController::update(Camera &camera, std::vector<Star> &stars) {
    // Update given by player
    camera.rotateLeft(motion.x);
    camera.rotateUp(motion.y);
    camera.moveLeft(motion.z);
    camera.moveFront(motion.w);
    motion = {0., 0., 0., 0.};

    // Update display data
    position = camera.getPosition();
    direction = normalize(camera.getFrontVector());

    sphereIntersect(stars);
    if (target != nullptr) {
        // Detect collision with target
        if (distanceToTarget <= 0.5) {
            float d = distance(position, target->getPosition());
            float f = (float) target->getRadius() / d;
            if (abs(d) > 1) {
                camera.setPosition(lerp(target->getPosition(), position, f + 0.02f));
            }
        }
    }
}

void PlayerController::sphereIntersect(std::vector<Star> &stars) {
    distanceToTarget = std::numeric_limits<double>::infinity();
    target = nullptr;
    for (Star& star : stars) {
        double distanceTo = star.distanceTo(position, direction);
        if (distanceTo < distanceToTarget) {
            distanceToTarget = distanceTo;
            target = &star;
        }
    }
}
