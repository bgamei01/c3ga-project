#include "../include/Program3D.hpp"

Program3D::Program3D(const char *execName) {
    glimac::FilePath applicationPath(execName);
    program = loadProgram(applicationPath.dirPath() + "shaders/3D.vs.glsl",
                          applicationPath.dirPath() + "shaders/color.fs.glsl");
    program.use();

    programId = program.getGLId();
    uColor = glGetUniformLocation(programId, "uColor");
    uMVPMatrix = glGetUniformLocation(programId, "uMVPMatrix");
}