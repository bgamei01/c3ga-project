#include "../include/Application.hpp"

#include <memory>
#include <iostream>
#include <fstream>
#include <glimac/common.hpp>
#include <glimac/Program.hpp>
#include <GL/glut.h>
#include <GL/freeglut.h>
#include <algorithm>

using namespace glm;
using namespace glimac;

Application::Application(const char *execName) {
    SDL_EnableKeyRepeat(100, 10);

    GLenum glewInitError = glewInit();

    if (GLEW_OK != glewInitError) {
        std::cerr << glewGetErrorString(glewInitError) << std::endl;
        exit(EXIT_FAILURE);
    }
    glEnable(GL_DEPTH_TEST);

    initVboVao();
    stars.emplace_back("Sun", vec3(0.98, 0.33, 0.20), vec3(0), c3ga::e13<double>(), 20);
    createRandomStars(execName);
    program = std::make_unique<Program3D>(execName);
    ProjMatrix = perspective(radians(70.f), (float)WIDTH / HEIGHT, 0.1f, 1000.f);
}

void Application::initVboVao() {
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glEnableVertexAttribArray(0);
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, unitSphere.getVertexCount() * sizeof(ShapeVertex),
                 unitSphere.getDataPointer(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex),
                          (const GLvoid *) offsetof(ShapeVertex, position));
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

vec3 hollowSphereRandom(const double r1, const double r2) {
    vec2 pt = diskRand((float) r2);
    if (pt.length() > r1) return vec3(pt, 0);
    pt += normalize(pt) * (float) r1;
    return {pt.x, 0, pt.y};
}

void Application::createRandomStars(const std::string &execName) {
    FilePath applicationPath(execName);
    std::ifstream planets{applicationPath.dirPath() + "data/planets.txt"};

    std::string line;
    std::vector<std::string> names;
    while (std::getline(planets, line)) {
        names.emplace_back(line);
    }
    for (const auto &name : names) {
        vec3 color = linearRand(vec3(0), vec3(1));
        vec3 position = hollowSphereRandom(50, 90);
        stars.emplace_back(name, color, position, c3ga::e13<double>() * glm::linearRand(-1.0, 1.0), linearRand(0.5, 1.0));
    }
}

void Application::run() {
    bool done = false;
    float previous{windowManager.getTime()}, dt{0}, current;
    while (!done) {
        // Event loop:
        SDL_Event e;
        while (windowManager.pollEvent(e)) {
            done = done || controller.treatEvent(e);
        }

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // - Update -
        controller.update(camera, stars);
        if (!controller.paused()) {
            // update star position
            for (auto &star : stars) {
                star.update(dt);
            }
            // Merge if colliding
            for (unsigned int i = 0; i < stars.size(); i++) {
                for (unsigned int j = i + 1; j < stars.size(); j++) {
                    if (stars[i].isCollidingWith(stars[j])) {
                        if (controller.hasTarget() && stars[j].getName() == controller.getTarget().getName()) {
                            controller.resetTarget();
                        }
                        stars.erase(stars.begin() + j);
                    }
                }
            }
            std::sort(stars.begin(), stars.end(),
                      [](const Star &a, const Star &b) { return a.getRadius() > b.getRadius(); });
        }
        // - Draw -
        glBindVertexArray(vao);
        for (const auto &star : stars) {
            draw_sphere(star.getPosition(), star.getColor(), star.getRadius());
        }
        // Debug point
        draw_sphere(controller.getPosition() + controller.getDirection(), vec3(1), .005);
        glBindVertexArray(0);
        drawGUI();

        // Update the display
        windowManager.swapBuffers();
        // Update time
        current = windowManager.getTime();
        if (!controller.paused()) {
            dt = current - previous;
        }
        previous = current;
    }
}

std::string viabilityFromColor(const vec3 &color, double &percentage) {
    double viability = -color.r + color.g + color.b;
    percentage = viability / 2. * 100.;
    if (viability < 0) {
        return "Extremely Low";
    }
    if (viability < 1) {
        return "Low";
    }
    return viability < 1.5 ? "Correct" : "Perfect";
}

void Application::drawGUI() const {
    const std::string sTemplate = "  --- Target ---  \n"
                                  "  Name : %s\n"
                                  "  Distance : %.2lf\n"
                                  "  Viability : %s (%.0lf%%)";
    if (!controller.hasTarget()) return;

    Star target = controller.getTarget();
    char *text = new char[sTemplate.size() + 100];
    double percentage;
    std::string viability = viabilityFromColor(target.getColor(), percentage);
    sprintf(text, sTemplate.data(), target.getName().data(), controller.getDistanceToTarget(), viability.data(),
            percentage);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glColor3f(1, 1, 1);
    glRasterPos2i(0, 0);
    glutBitmapString(GLUT_BITMAP_TIMES_ROMAN_10, (unsigned char *) text);

    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
}


void Application::draw_sphere(const vec3 &position, const vec3 &color, double radius) const {
    mat4 M = translate(camera.getViewMatrix(), position);
    M = scale(M, vec3(radius));
    glUniformMatrix4fv(program->uMVPMatrix, 1, GL_FALSE, value_ptr(ProjMatrix * M));
    glUniform3fv(program->uColor, 1, value_ptr(color));
    glDrawArrays(GL_TRIANGLES, 0, unitSphere.getVertexCount());
}
